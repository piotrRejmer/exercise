/**
 * Created by nauczyciel on 2015-10-28.
 */
function showPage(cls){
    if ($('.page.act').is(cls)) return; //Nic nie robi� je�eli wybrana ta sama strona

    $('.page.act').slideUp().removeClass('act');
    $(cls).slideDown().addClass('act');
}

$(function(){
    $('.act').show();


    $('.sel-page1').click(function(){
        showPage('.page1');
    });
    $('.sel-page2').click(function(){
        showPage('.page2');
    });
    $('.sel-page3').click(function(){
        showPage('.page3');
    });
    $('.sel-page4').click(function(){
        showPage('.page4');
    });
    $('.sel-page5').click(function(){
        showPage('.page5');
    });
    $('.sel-page6').click(function(){
        showPage('.page6');
    });
    $('.sel-page7').click(function(){
        showPage('.page7');
    });
});